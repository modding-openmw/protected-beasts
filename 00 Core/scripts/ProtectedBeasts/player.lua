    local core = require('openmw.core')
    local types = require('openmw.types')
    local interfaces = require('openmw.interfaces')
    local self = require("openmw.self")
    local Actor = types.Actor

    local function PB_setEquipment(equip)
        Actor.setEquipment(self, equip)
        interfaces.UI.setMode(nil)
        core.sendGlobalEvent("RemoveItemNow")
    end

    local function OpenAgain()
        interfaces.UI.setMode(interfaces.UI.MODE.Interface)
    end

return {
    eventHandlers = {
        PB_setEquipment = PB_setEquipment,
        OpenAgain = OpenAgain,
    }
}

local core = require('openmw.core')
local MOD_ID = "ProtectedBeasts"
local L = core.l10n(MOD_ID)

if core.API_REVISION < 30 then
    error(L("needNewerOpenMW"))
end

FoundArgonianFullHelms = false
FoundBootsforBeasts = false
FoundTamrielData = false

-- Check if we have any content files that we can use
if core.contentFiles.has("Tamriel_Data.esm") then
    FoundTamrielData = true
    print(L("haveTamrielData"))
end

if core.contentFiles.has("Clean_Argonian Full Helms Lore Integrated.ESP")
    or core.contentFiles.has("Argonian Full Helms OpenMW-Lua.ESP") then
        FoundArgonianFullHelms = true
        print(L("haveArgonianFullHelms"))
end

if core.contentFiles.has("Boots for Beasts-GOTY-OpenMW Version.esp")
    or core.contentFiles.has("Boots for Beasts OpenMW Lua.esp")then
        FoundBootsforBeasts = true
        print(L("haveBootsforBeasts"))
end

if (FoundArgonianFullHelms == false) and (FoundBootsforBeasts == false) then
    error(L("missingDep"))
end
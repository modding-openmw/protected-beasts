require("scripts.ProtectedBeasts.checks")
local types    = require("openmw.types")
local world    = require("openmw.world")
local interfaces = require("openmw.interfaces")
local csv      = require("scripts.ProtectedBeasts.csv_reader")

local csvFiles = csv.getCSVTables()

local function findSlot(item)
    if (item == nil) then
        return
    end
    --Finds a equipment slot for an inventory item, if it has one,
    if item.type == types.Armor then
        if (types.Armor.record(item).type == types.Armor.TYPE.RGauntlet) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Armor.record(item).type == types.Armor.TYPE.LGauntlet) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.record(item).type == types.Armor.TYPE.Boots) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Armor.record(item).type == types.Armor.TYPE.Cuirass) then
            return types.Actor.EQUIPMENT_SLOT.Cuirass
        elseif (types.Armor.record(item).type == types.Armor.TYPE.Greaves) then
            return types.Actor.EQUIPMENT_SLOT.Greaves
        elseif (types.Armor.record(item).type == types.Armor.TYPE.LBracer) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Armor.record(item).type == types.Armor.TYPE.RBracer) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.record(item).type == types.Armor.TYPE.RPauldron) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Armor.record(item).type == types.Armor.TYPE.LPauldron) then
            return types.Actor.EQUIPMENT_SLOT.LeftPauldron
        elseif (types.Armor.record(item).type == types.Armor.TYPE.RPauldron) then
            return types.Actor.EQUIPMENT_SLOT.RightPauldron
        elseif (types.Armor.record(item).type == types.Armor.TYPE.Helmet) then
            return types.Actor.EQUIPMENT_SLOT.Helmet
        end
    elseif item.type == types.Clothing then
        if (types.Clothing.record(item).type == types.Clothing.TYPE.Amulet) then
            return types.Actor.EQUIPMENT_SLOT.Amulet
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Belt) then
            return types.Actor.EQUIPMENT_SLOT.Belt
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.LGlove) then
            return types.Actor.EQUIPMENT_SLOT.LeftGauntlet
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.RGlove) then
            return types.Actor.EQUIPMENT_SLOT.RightGauntlet
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Ring) then
            return types.Actor.EQUIPMENT_SLOT.RightRing
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Skirt) then
            return types.Actor.EQUIPMENT_SLOT.Skirt
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Shirt) then
            return types.Actor.EQUIPMENT_SLOT.Shirt
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Shoes) then
            return types.Actor.EQUIPMENT_SLOT.Boots
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Robe) then
            return types.Actor.EQUIPMENT_SLOT.Robe
        elseif (types.Clothing.record(item).type == types.Clothing.TYPE.Pants) then
            return types.Actor.EQUIPMENT_SLOT.Pants
        end
    elseif item.type == types.Weapon then
        if (item.type.record(item).type == types.Weapon.TYPE.Arrow or item.type.record(item).type == types.Weapon.TYPE.Bolt) then
            return types.Actor.EQUIPMENT_SLOT.Ammunition
        end
        return types.Actor.EQUIPMENT_SLOT.CarriedRight
    end
    return nil
end
local itemToRemove
local actorToRemove

local function argonian_full_helms(actor)
    local race = types.NPC.record(actor).race
    local beastRaces = {
        ["argonian"] = true,
    }
    if beastRaces[race] then
        return beastRaces[race]
    end
    return false
end

local function argonian_full_helms_td(actor)
    local race = types.NPC.record(actor).race
    local beastRaces = {
        ["argonian"] = true,
    }
    if beastRaces[race] then
        return beastRaces[race]
    end
    return false
end

local function boots_for_beasts(actor)
    local race = types.NPC.record(actor).race
    local beastRaces = {
        ["argonian"] = true,
        ["khajiit"] = true,
        ["t_els_cathay-raht"] = true,
        ["t_els_dagi-raht"] = true
    }
    if beastRaces[race] then
        return beastRaces[race]
    end
    return false
end

local function boots_for_beasts_td(actor)
    local race = types.NPC.record(actor).race
    local beastRaces = {
        ["argonian"] = true,
        ["khajiit"] = true,
        ["t_els_cathay-raht"] = true,
        ["t_els_dagi-raht"] = true
    }
    if beastRaces[race] then
        return beastRaces[race]
    end
    return false
end

local useNewID = {}
if (FoundArgonianFullHelms) then
    useNewID["argonian_full_helms"] = argonian_full_helms
    if (FoundTamrielData) then
        useNewID["argonian_full_helms_td"] = argonian_full_helms_td
    end
end

if (FoundBootsforBeasts) then
    useNewID["boots_for_beasts"] = boots_for_beasts
    if (FoundTamrielData) then
        useNewID["boots_for_beasts_td"] = boots_for_beasts_td
    end
end

interfaces.ItemUsage.addHandlerForType(types.Armor, function(armor, actor)
    if not armor then return end

    local record = types.Armor.record(armor)

    for fileName, file in pairs(csvFiles) do
        local mseNewID = false
        if useNewID[fileName] then
            mseNewID = useNewID[fileName](actor)
        end
        for key, data in ipairs(file) do
            if data.baseid:lower() == armor.recordId and mseNewID == true  and data.itemtype == nil then
                local check = types.Armor.record(data.newid)
                if check then
                    local newArmor = world.createObject(tostring(data.newid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("PB_setEquipment", eq)
                    return false
                end
            elseif data.newid:lower() == armor.recordId and mseNewID == false and data.itemtype == nil then
                local check = types.Armor.record(data.newid)
                if check then
                    local newArmor = world.createObject(tostring(data.baseid), 1)
                    newArmor:moveInto(actor)
                    local eq = types.Actor.getEquipment(actor)
                    eq[findSlot(newArmor)] = newArmor.recordId
                    itemToRemove = armor.recordId
                    actorToRemove = actor
                    actor:sendEvent("PB_setEquipment", eq)
                    return false
                end
            end
        end
    end
end)

local function RemoveItemNow()
    for index, value in ipairs(types.Actor.inventory(actorToRemove):getAll()) do
        if value.recordId == itemToRemove then
            value:remove(1)
            actorToRemove:sendEvent("OpenAgain")
            return
        end
    end
end

return {
    interfaceName = "protectedbeasts",
    interface = {
        version = 1,
        csvFiles = csvFiles,
    },
    eventHandlers = {
        RemoveItemNow = RemoveItemNow
    }
}
